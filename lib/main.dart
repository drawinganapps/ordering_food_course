import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:food_ordering_course/bloc/cart/cart_bloc.dart';
import 'package:food_ordering_course/bloc/item/item_bloc.dart';
import 'package:food_ordering_course/repositories/item_repository.dart';
import 'package:food_ordering_course/repositories/simple_bloc_observer.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'routes/AppPages.dart';
import 'themes/AppTheme.dart';

void main() {
  Bloc.observer = SimpleBlocObserver();
  runApp(MyApp(itemRepository: ItemRepository()));
}

class MyApp extends StatelessWidget {
  final ItemRepository itemRepository;

  const MyApp({Key? key, required this.itemRepository}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.immersiveSticky,
        overlays: []);

    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
    ));

    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (_) => ItemBloc(
            itemRepository: itemRepository,
          )..add(ItemStarted()),
        ),
        BlocProvider(
          create: (_) => CartBloc()..add(CartStarted()),
        )
      ],
      child: GlobalLoaderOverlay(
        child: MaterialApp.router(
          routerConfig: AppPage.routes,
          theme: AppTheme.dark,
          themeMode: ThemeMode.system,
          debugShowCheckedModeBanner: false,
        ),
      ),
    );
  }
}
