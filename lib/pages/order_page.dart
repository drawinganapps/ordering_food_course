import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:food_ordering_course/bloc/cart/cart_bloc.dart';
import 'package:food_ordering_course/helper/color_helper.dart';
import 'package:food_ordering_course/helper/total_calulation_helper.dart';
import 'package:food_ordering_course/routes/AppRoutes.dart';
import 'package:food_ordering_course/widgets/apply_coupon_widget.dart';
import 'package:food_ordering_course/widgets/default_loading_widget.dart';
import 'package:food_ordering_course/widgets/empty_cart_widget.dart';
import 'package:food_ordering_course/widgets/order_item_card_widget.dart';
import 'package:go_router/go_router.dart';
import 'package:loader_overlay/loader_overlay.dart';

class OrderPage extends StatelessWidget {
  const OrderPage({super.key});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    return BlocBuilder<CartBloc, CartState>(builder: (context, state) {
      if (state is CartLoaded && state.carts.isNotEmpty) {
        return ListView(
          physics: const BouncingScrollPhysics(),
          children: [
            Container(
                height: heightSize * 0.48,
                margin: EdgeInsets.only(top: heightSize * 0.01),
                padding: EdgeInsets.only(
                    left: widthSize * 0.05, right: widthSize * 0.05),
                child: ListView(
                  padding: const EdgeInsets.all(0),
                  physics: const BouncingScrollPhysics(),
                  children: List.generate(state.carts.length, (index) {
                    return Container(
                      margin: EdgeInsets.only(bottom: heightSize * 0.025),
                      child: OrderItemCardWidget(cart: state.carts[index]),
                    );
                  }),
                )),
            Container(
              padding: EdgeInsets.only(
                  left: widthSize * 0.05, right: widthSize * 0.05),
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(bottom: heightSize * 0.03),
                    padding: EdgeInsets.only(
                        left: widthSize * 0.08, right: widthSize * 0.08),
                    child: Container(
                        decoration: BoxDecoration(
                            border: Border(
                                top: BorderSide(
                                    color:
                                        ColorHelper.grey.withOpacity(0.4))))),
                  ),
                  const ApplyCouponWidget()
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(
                  left: widthSize * 0.08, right: widthSize * 0.08),
              margin: EdgeInsets.only(
                  top: heightSize * 0.03, bottom: heightSize * 0.03),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('Total',
                      style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w600,
                          color: ColorHelper.yellow)),
                  Text(
                      '\$ ${TotalCalculationHelper.grandTotal(state.carts).toStringAsFixed(2)}',
                      style: TextStyle(
                          fontSize: 22,
                          fontWeight: FontWeight.w600,
                          color: ColorHelper.yellow)),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(
                  left: widthSize * 0.05, right: widthSize * 0.05),
              child: GestureDetector(
                onTap: () {
                  context.loaderOverlay
                      .show(widget: const DefaultLoadingWidget());
                  Future.delayed(const Duration(milliseconds: 1000), () {
                    context.go(AppRoutes.ORDER_COMPLETE);
                    context.loaderOverlay.hide();
                  });
                },
                child: Container(
                    height: heightSize * 0.08,
                    decoration: BoxDecoration(
                        color: ColorHelper.yellow,
                        borderRadius: BorderRadius.circular(20)),
                    child: Center(
                      child: Text('Proceed to Checkout',
                          style: TextStyle(
                              color: ColorHelper.white,
                              fontSize: 22,
                              fontWeight: FontWeight.w600)),
                    )),
              ),
            ),
          ],
        );
      }
      return const EmptyCartWidget();
    });
  }
}
