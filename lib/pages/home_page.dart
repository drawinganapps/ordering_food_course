import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:food_ordering_course/bloc/item/item_bloc.dart';
import 'package:food_ordering_course/helper/color_helper.dart';
import 'package:food_ordering_course/widgets/default_loading_widget.dart';
import 'package:food_ordering_course/widgets/filter_widget.dart';
import 'package:food_ordering_course/widgets/item_card_widget.dart';
import 'package:food_ordering_course/widgets/search_widget.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    return ListView(
      physics: const BouncingScrollPhysics(),
      children: [
        Container(
          margin: EdgeInsets.only(top: heightSize * 0.03),
          padding:
              EdgeInsets.only(left: widthSize * 0.05, right: widthSize * 0.05),
          child: const SearchWidget(),
        ),
        SizedBox(
          height: heightSize * 0.1,
          child: BlocBuilder<ItemBloc, ItemState>(builder: (context, state) {
            if (state is ItemLoaded) {
              return ListView(
                  physics: const BouncingScrollPhysics(),
                  scrollDirection: Axis.horizontal,
                  children: List.generate(state.categories.length, (index) {
                    return GestureDetector(
                      onTap: () {
                        context.read<ItemBloc>().add(CategorySelected(
                            category: state.categories[index]));
                      },
                      child: FilterWidget(
                          isSelected: state.categories[index].id ==
                              state.selectedCategory.id,
                          filter: state.categories[index]),
                    );
                  }));
            }

            return Container();
          }),
        ),
        Container(
            margin: EdgeInsets.only(bottom: heightSize * 0.03),
            padding: EdgeInsets.only(
                left: widthSize * 0.05, right: widthSize * 0.05),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Recommended Food',
                    style: TextStyle(
                        color: ColorHelper.white,
                        fontSize: 16,
                        fontWeight: FontWeight.w600)),
                Text('See all',
                    style: TextStyle(
                        color: ColorHelper.grey,
                        fontSize: 14,
                        fontWeight: FontWeight.w600))
              ],
            )),
        Container(
            padding: EdgeInsets.only(
                left: widthSize * 0.05, right: widthSize * 0.05),
            child: BlocBuilder<ItemBloc, ItemState>(
              builder: (context, state) {
                if (state is ItemLoaded) {
                  return GridView.count(
                    shrinkWrap: true,
                    crossAxisCount: 2,
                    crossAxisSpacing: 15,
                    mainAxisSpacing: 15,
                    childAspectRatio: 0.75,
                    physics: const BouncingScrollPhysics(),
                    children:
                        List.generate(state.filteredItems.length, (index) {
                      return ItemCardWidget(
                        item: state.filteredItems[index],
                      );
                    }),
                  );
                }
                return const DefaultLoadingWidget();
              },
            ))
      ],
    );
  }
}
