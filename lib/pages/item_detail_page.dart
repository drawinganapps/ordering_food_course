import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:food_ordering_course/bloc/item/item_bloc.dart';
import 'package:food_ordering_course/helper/color_helper.dart';
import 'package:food_ordering_course/models/item_model.dart';
import 'package:food_ordering_course/sources/dummy_data.dart';

class ItemDetailPage extends StatelessWidget {
  const ItemDetailPage({super.key});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;

    return BlocBuilder<ItemBloc, ItemState>(builder: (context, state) {
      if (state is ItemLoaded) {
        ItemModel item = state.selectedItem;
        return ListView(
          physics: const BouncingScrollPhysics(),
          padding: const EdgeInsets.all(0),
          children: [
            Container(
              height: heightSize * 0.42,
              padding: EdgeInsets.only(
                  left: widthSize * 0.05, right: widthSize * 0.05),
              decoration: BoxDecoration(
                  color: ColorHelper.secondary,
                  borderRadius: const BorderRadius.only(
                    bottomRight: Radius.circular(50),
                    bottomLeft: Radius.circular(50),
                  )),
              child: Image.asset(
                'assets/images/${item.icon}',
                fit: BoxFit.cover,
              ),
            ),
            Container(
              padding: EdgeInsets.only(
                  left: widthSize * 0.05,
                  right: widthSize * 0.05,
                  bottom: heightSize * 0.03,
                  top: heightSize * 0.05),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(item.name,
                          style: TextStyle(
                              color: ColorHelper.white,
                              fontSize: 22,
                              fontWeight: FontWeight.w600)),
                      Container(
                        margin: EdgeInsets.only(
                          top: heightSize * 0.02,
                          bottom: heightSize * 0.02,
                        ),
                        child: Text(DummyData.description,
                            style: TextStyle(
                              fontSize: 16,
                              height: 1.5,
                              overflow: TextOverflow.fade,
                              color: ColorHelper.white.withOpacity(0.8),
                            ),
                            textAlign: TextAlign.justify,
                            maxLines: 4),
                      ),
                      Row(
                        children: [
                          Row(
                            children: [
                              Container(
                                margin:
                                EdgeInsets.only(right: widthSize * 0.02),
                                child: Icon(Icons.local_activity,
                                    color: ColorHelper.grey),
                              ),
                              Text(item.rate.toString(),
                                  style: TextStyle(color: ColorHelper.grey))
                            ],
                          ),
                          Container(
                            margin: EdgeInsets.only(left: widthSize * 0.1),
                          ),
                          Row(
                            children: [
                              Container(
                                margin:
                                EdgeInsets.only(right: widthSize * 0.02),
                                child: Icon(Icons.location_on,
                                    color: ColorHelper.grey),
                              ),
                              Text('Denpasar',
                                  style: TextStyle(color: ColorHelper.grey))
                            ],
                          )
                        ],
                      ),
                      Container(
                        margin: EdgeInsets.only(top: heightSize * 0.03),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('Price',
                                style: TextStyle(
                                    fontSize: 20,
                                    color: ColorHelper.white.withOpacity(0.8))),
                            Text('\$  ${item.price.toString()}',
                                style: TextStyle(
                                    fontSize: 28,
                                    fontWeight: FontWeight.w600,
                                    color: ColorHelper.white)),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            )
          ],
        );
      }
      return Container();
    });
  }
}
