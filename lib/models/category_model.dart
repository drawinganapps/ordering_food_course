class CategoryModel {
  int id;
  String name;
  String filter;
  String icon;

  CategoryModel(this.id, this.name, this.filter, this.icon);
}
