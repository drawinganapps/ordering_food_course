import 'package:food_ordering_course/models/category_model.dart';

class ItemModel {
  int id;
  String name;
  String icon;
  double price;
  double rate;
  CategoryModel category;

  ItemModel(this.id, this.name, this.icon, this.price, this.rate, this.category);
}
