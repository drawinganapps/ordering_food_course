import 'package:food_ordering_course/models/item_model.dart';

class CartModel {
  ItemModel item;
  int quantity;

  CartModel({required this.item, required this.quantity});
}
