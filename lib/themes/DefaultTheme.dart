import 'package:flutter/material.dart';
import 'package:food_ordering_course/helper/color_helper.dart';
import 'package:google_fonts/google_fonts.dart';

ThemeData defaultTheme = ThemeData(
    brightness: Brightness.dark,
    backgroundColor: ColorHelper.primary,
    scaffoldBackgroundColor: ColorHelper.primary,
    highlightColor: ColorHelper.primary,
    splashColor: ColorHelper.primary,
    primarySwatch: Colors.amber,
    textTheme: GoogleFonts.arimoTextTheme().copyWith(
    ),
);
