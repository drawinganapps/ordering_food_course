import 'package:food_ordering_course/models/cart_model.dart';

class TotalCalculationHelper {
  static double grandTotal(List<CartModel> carts) {
    double total = 0.0;

    for (var element in carts) {
      double price = element.item.price;
      total += price * element.quantity;
    }

    return total;
  }
}
