import 'package:flutter/material.dart';

class ColorHelper {
  static Color white = const Color.fromRGBO(255,255,255, 1);
  static Color grey = const Color.fromRGBO(110, 110, 110, 1);
  static Color primary = const Color.fromRGBO(37, 37, 37, 1);
  static Color secondary = const Color.fromRGBO(43, 38, 38, 1.0);
  static Color tertiary = const Color.fromRGBO(52, 44, 35, 1);
  static Color quaternary = const Color.fromRGBO(80, 59, 34, 1);
  static Color yellow = const Color.fromRGBO(255, 144, 18, 1);
}
