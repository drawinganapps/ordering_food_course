import 'package:food_ordering_course/models/category_model.dart';
import 'package:food_ordering_course/models/item_model.dart';
import 'package:food_ordering_course/sources/dummy_data.dart';

const _delay = Duration(milliseconds: 1000);

class ItemRepository {

  Future<List<ItemModel>> getAllItems() async {
    return Future.delayed(_delay, () => DummyData.items);
  }

  Future<List<CategoryModel>> getAllCategories() async {
    return Future.delayed(_delay, () => DummyData.categories);
  }
}
