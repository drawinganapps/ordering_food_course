import 'package:food_ordering_course/models/category_model.dart';
import 'package:food_ordering_course/models/item_model.dart';

class DummyData {
  static List<CategoryModel> categories = [
    CategoryModel(1, 'All', 'all', 'all.png'),
    CategoryModel(2, 'Breakfast', 'breakfast', 'breakfast.png'),
    CategoryModel(3, 'Drinks', 'drinks','cola.png'),
    CategoryModel(4, 'Snack', 'snack', 'snack.png'),
  ];

  static List<ItemModel> items = [
    ItemModel(1, 'Ramen', 'ramen.png', 8.2, 4.6, categories[1]),
    ItemModel(2, 'Miso Nugget', 'miso.png', 6.7, 4.5, categories[1]),
    ItemModel(3, 'Salmon Sushi', 'sushi.png', 11.9, 4.7, categories[1]),
    ItemModel(4, 'Katsu', 'katsu.png', 9.9, 4.3, categories[1]),
    ItemModel(5, 'Pizza', 'pizza.png', 11.2, 4.8, categories[1]),
    ItemModel(6, 'Suhsi Roll', 'sushi-roll.png', 9.9, 4.7, categories[1]),
    ItemModel(7, 'Cola', 'cola.png', 1.5, 4.8, categories[2]),
    ItemModel(8, 'Pepsi', 'pepsi.png', 1.5, 4.8, categories[2]),
    ItemModel(9, 'French fries', 'potato.png', 4.5, 4.6, categories[3]),
    ItemModel(10, 'Ice Cream', 'icecream.png', 2.2, 4.5, categories[3]),
  ];

  static String description = """You boil some water while you desperately tear open the packet. The water bubbles away madly as you reach to cut the taste maker within the two-minute noodle packet.
  \nTwo minutes, maybe three minutes later, you’re burning your tongue as you wallop down the noodles. What you’ve experienced is an instant result.
  And readers too want to experience that very same instant result with your content. They don’t want to waffle through mountains of information. They want a nice, quick bite to begin with. Of course, it helps if you have the right parameters in place.
  """;
}
