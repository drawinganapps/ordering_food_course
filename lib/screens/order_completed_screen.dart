import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:food_ordering_course/bloc/cart/cart_bloc.dart';
import 'package:food_ordering_course/helper/color_helper.dart';
import 'package:food_ordering_course/routes/AppRoutes.dart';
import 'package:food_ordering_course/widgets/default_loading_widget.dart';
import 'package:go_router/go_router.dart';
import 'package:loader_overlay/loader_overlay.dart';

class OrderCompletedScreen extends StatelessWidget {
  const OrderCompletedScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset('assets/icons/check.png', width: 100, height: 100)
            ],
          ),
          Container(
            margin: const EdgeInsets.only(top: 10, bottom: 20),
            child: Text('Yor order has been placed!',
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.w900, color: ColorHelper.white)),
          ),
          Container(
            padding: EdgeInsets.only(
                left: widthSize * 0.06, right: widthSize * 0.06),
            child: Text('Thank you, we will serve your order immediately.',
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontStyle: FontStyle.italic,
                    color: ColorHelper.white,
                    fontWeight: FontWeight.w500,
                    fontSize: 18)),
          )
        ],
      ),
      floatingActionButton: Container(
        height: 60,
        width: widthSize * 0.8,
        padding: const EdgeInsets.only(left: 30, right: 30),
        decoration: BoxDecoration(
            color: ColorHelper.yellow, borderRadius: BorderRadius.circular(10)),
        child: Center(
          child: GestureDetector(
            onTap: (() {
              context.loaderOverlay.show(widget: const DefaultLoadingWidget());
              Future.delayed(const Duration(seconds: 1), () {
                context.read<CartBloc>().add(CartItemCleared());
                context.loaderOverlay.hide();
                context.go(AppRoutes.HOME);
              });
            }),
            child: Text('Got It',
                style: TextStyle(
                    color: ColorHelper.white,
                    fontSize: 18,
                    fontWeight: FontWeight.w900)),
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
