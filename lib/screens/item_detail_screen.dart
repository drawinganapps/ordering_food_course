import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:food_ordering_course/bloc/cart/cart_bloc.dart';
import 'package:food_ordering_course/bloc/item/item_bloc.dart';
import 'package:food_ordering_course/helper/color_helper.dart';
import 'package:food_ordering_course/pages/item_detail_page.dart';
import 'package:food_ordering_course/routes/AppRoutes.dart';
import 'package:food_ordering_course/widgets/item_detail_action_widget.dart';
import 'package:go_router/go_router.dart';

class ItemDetailScreen extends StatelessWidget {
  const ItemDetailScreen({super.key});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: ColorHelper.secondary,
          leadingWidth: widthSize * 0.2,
          leading: Center(
            child: GestureDetector(
              child: Container(
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                    color: ColorHelper.tertiary,
                    borderRadius: BorderRadius.circular(10)),
                child: Icon(Icons.arrow_back_rounded, color: ColorHelper.white),
              ),
              onTap: () => context.go(AppRoutes.HOME),
            ),
          ),
          actions: [
            Center(
              child: GestureDetector(
                onTap: () => context.go(AppRoutes.ORDERS),
                child: Container(
                  margin: EdgeInsets.only(right: widthSize * 0.05),
                  padding: const EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      color: ColorHelper.tertiary,
                      borderRadius: BorderRadius.circular(10)),
                  child: Icon(Icons.shopping_basket, color: ColorHelper.yellow),
                ),
              ),
            ),
          ],
        ),
        body: const ItemDetailPage(),
        bottomNavigationBar:
            BlocBuilder<ItemBloc, ItemState>(builder: (context, state) {
          if (state is ItemLoaded) {
            return ItemDetailActionWidget(itemModel: state.selectedItem);
          }
          return Container();
        }));
  }
}
