import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:food_ordering_course/helper/color_helper.dart';
import 'package:food_ordering_course/pages/home_page.dart';
import 'package:food_ordering_course/routes/AppRoutes.dart';
import 'package:food_ordering_course/widgets/navigation_widget.dart';
import 'package:go_router/go_router.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorHelper.primary,
        elevation: 0,
        title: Container(
          margin: EdgeInsets.only(left: widthSize * 0.02),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Text('Hey Mirlan',
                      style: TextStyle(
                          color: ColorHelper.yellow,
                          fontSize: 20,
                          fontWeight: FontWeight.w600)),
                  Container(
                    margin: EdgeInsets.only(left: widthSize * 0.02),
                    child: Icon(Icons.waving_hand_rounded,
                        size: 16, color: ColorHelper.yellow),
                  )
                ],
              ),
              Container(
                margin: EdgeInsets.only(top: heightSize * 0.01),
                child: Text('Find fresh food you want!',
                    style: TextStyle(
                        color: ColorHelper.grey,
                        fontSize: 14,
                        fontWeight: FontWeight.w600)),
              )
            ],
          ),
        ),
        actions: [
          Container(
            margin: EdgeInsets.only(right: widthSize * 0.05),
            child: Center(
              child: GestureDetector(
                child: Container(
                  padding: const EdgeInsets.all(7),
                  decoration: BoxDecoration(
                      color: ColorHelper.tertiary,
                      borderRadius: BorderRadius.circular(10)),
                  child: Icon(Icons.shopping_basket, color: ColorHelper.yellow),
                ),
                onTap: () => context.go(AppRoutes.ORDERS),
              ),
            ),
          )
        ],
      ),
      body: const HomePage(),
      bottomNavigationBar: const NavigationWidget(selectedMenu: 0),
    );
  }
}
