import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:food_ordering_course/models/cart_model.dart';
import 'package:food_ordering_course/models/item_model.dart';

part 'cart_event.dart';

part 'cart_state.dart';

class CartBloc extends Bloc<CartEvent, CartState> {
  CartBloc() : super(CartLoading()) {
    on<CartStarted>(_onCartStarted);
    on<CartItemAdded>(_onItemAdded);
    on<CartItemDecreased>(_onItemDecreased);
    on<CartItemIncreased>(_onItemIncreased);
    on<CartItemCleared>(_onCartItemCleared);
  }

  Future<void> _onCartStarted(
      CartStarted event, Emitter<CartState> emit) async {
    emit(CartLoading());
    try {
      emit(const CartLoaded(carts: []));
    } catch (_) {
      emit(CartError());
    }
  }

  Future<void> _onItemAdded(
      CartItemAdded event, Emitter<CartState> emit) async {
    final state = this.state;
    if (state is CartLoaded) {
      try {
        var currentItemCart = state.carts
            .indexWhere((element) => element.item.id == event.cart.item.id);
        if (currentItemCart > -1) {
          state.carts[currentItemCart].quantity += event.cart.quantity;
          emit(CartLoaded(carts: [...state.carts]));
          return;
        }
        emit(CartLoaded(carts: [...state.carts, event.cart]));
      } catch (_) {
        emit(CartError());
      }
    }
  }

  Future<void> _onItemDecreased(
      CartItemDecreased event, Emitter<CartState> emit) async {
    final state = this.state;
    if (state is CartLoaded) {
      try {
        var carts = state.carts;
        var currentItemCart = state.carts
            .firstWhere((element) => element.item.id == event.item.id);
        if (currentItemCart.quantity > 1) {
          var newDecreasedCart = CartModel(
              item: currentItemCart.item,
              quantity: currentItemCart.quantity -= 1);
          var newCarts = state.carts.map((e) {
            if (e.item.id == newDecreasedCart.item.id) {
              return newDecreasedCart;
            }
            return e;
          }).toList();
          emit(CartLoaded(carts: newCarts));
          return;
        }
        emit(CartLoaded(
            carts: [...carts]
              ..removeWhere((element) => element.item.id == event.item.id)));
      } catch (_) {
        emit(CartError());
      }
    }
  }

  Future<void> _onItemIncreased(
      CartItemIncreased event, Emitter<CartState> emit) async {
    final state = this.state;
    if (state is CartLoaded) {
      try {
        var currentItemCart = state.carts
            .firstWhere((element) => element.item.id == event.item.id);
        var newIncreasedCart = CartModel(
            item: currentItemCart.item,
            quantity: currentItemCart.quantity += 1);
        var newCarts = state.carts.map((e) {
          if (e.item.id == newIncreasedCart.item.id) {
            return newIncreasedCart;
          }
          return e;
        }).toList();
        emit(CartLoaded(carts: newCarts));
      } catch (_) {
        emit(CartError());
      }
    }
  }

  Future<void> _onCartItemCleared(CartItemCleared event, Emitter<CartState> emit) async {
    try {
      emit(const CartLoaded(carts: []));
    } catch(_){
      emit(CartError());
    }
  }
}
