part of 'cart_bloc.dart';

abstract class CartEvent extends Equatable {
  const CartEvent();
}

class CartStarted extends CartEvent {
  @override
  List<Object> get props => [];
}

class CartItemAdded extends CartEvent {

  final CartModel cart;

  const CartItemAdded({required this.cart});

  @override
  List<Object> get props => [cart];
}

class CartItemDecreased extends CartEvent {

  final ItemModel item;

  const CartItemDecreased({required this.item});

  @override
  List<Object> get props => [item];
}

class CartItemIncreased extends CartEvent {

  final ItemModel item;

  const CartItemIncreased({required this.item});

  @override
  List<Object> get props => [item];
}

class CartItemCleared extends CartEvent {

  @override
  List<Object> get props => [];
}