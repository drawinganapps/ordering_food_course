part of 'cart_bloc.dart';

abstract class CartState extends Equatable {
  const CartState();

  @override
  List<Object> get props => [];
}

class CartLoading extends CartState {}

class CartLoaded extends CartState {
  const CartLoaded({required this.carts});

  final List<CartModel> carts;

  @override
  List<Object> get props => [
    carts
  ];
}

class CartError extends CartState {}
