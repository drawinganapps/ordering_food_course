import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:food_ordering_course/models/category_model.dart';
import 'package:food_ordering_course/models/item_model.dart';
import 'package:food_ordering_course/repositories/item_repository.dart';

part 'item_event.dart';
part 'item_state.dart';

class ItemBloc extends Bloc<ItemEvent, ItemState> {
  final ItemRepository itemRepository;

  ItemBloc({required this.itemRepository}) : super(ItemLoading()) {
    on<ItemStarted>(_onStarted);
    on<ItemSelected>(_onItemSelected);
    on<CategorySelected>(_onCategorySelected);
  }

  Future<void> _onStarted(ItemStarted event, Emitter<ItemState> emit,) async {
    emit(ItemLoading());
    try {
      final items = await itemRepository.getAllItems();
      final categories = await itemRepository.getAllCategories();
      emit(ItemLoaded(items: items, categories: categories, selectedCategory: categories[0], selectedItem: items[0]));
    } catch (_) {
      emit(ItemError());
    }
  }

  Future<void> _onItemSelected(ItemSelected event, Emitter<ItemState> emit) async {
    final state = this.state;
    if (state is ItemLoaded) {
      try {
        emit(ItemLoaded(items: state.items, categories: state.categories, selectedCategory: state.selectedCategory, selectedItem: event.item));
      } catch (_) {
        emit(ItemError());
      }
    }
  }

  Future<void> _onCategorySelected(CategorySelected event, Emitter<ItemState> emit) async {
    final state = this.state;
    if (state is ItemLoaded) {
      try {
        emit(ItemLoaded(items: state.items, categories: state.categories, selectedCategory: event.category, selectedItem: state.selectedItem));
      } catch (_) {
        emit(ItemError());
      }
    }
  }
}
