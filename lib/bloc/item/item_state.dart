part of 'item_bloc.dart';

abstract class ItemState extends Equatable {
  const ItemState();

  @override
  List<Object> get props => [];
}

class ItemLoading extends ItemState {}

class ItemLoaded extends ItemState {
  const ItemLoaded(
      {required this.items,
      required this.categories,
      required this.selectedCategory,
      required this.selectedItem});

  final List<ItemModel> items;
  final List<CategoryModel> categories;
  final CategoryModel selectedCategory;
  final ItemModel selectedItem;

  List<ItemModel> get filteredItems => items.where((element) {
        if (selectedCategory.filter == 'all') {
          return true;
        }

        return element.category.id == selectedCategory.id;
      }).toList();

  @override
  List<Object> get props => [items, categories, selectedCategory, selectedItem];
}

class ItemError extends ItemState {}
