part of 'item_bloc.dart';

abstract class ItemEvent extends Equatable {
  const ItemEvent();
}

class ItemStarted extends ItemEvent {
  @override
  List<Object> get props => [];
}

class ItemSelected extends ItemEvent {
  const ItemSelected({required this.item});

  final ItemModel item;

  @override
  List<Object> get props => [item];
}

class CategorySelected extends ItemEvent {
  const CategorySelected({required this.category});

  final CategoryModel category;

  @override
  List<Object> get props => [category];
}
