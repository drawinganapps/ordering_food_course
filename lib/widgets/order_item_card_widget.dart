import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:food_ordering_course/bloc/cart/cart_bloc.dart';
import 'package:food_ordering_course/helper/color_helper.dart';
import 'package:food_ordering_course/models/cart_model.dart';

class OrderItemCardWidget extends StatelessWidget {
  final CartModel cart;

  const OrderItemCardWidget({super.key, required this.cart});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    return Container(
      height: heightSize * 0.12,
      padding: EdgeInsets.only(left: widthSize * 0.03, right: widthSize * 0.03),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(25),
          color: ColorHelper.secondary),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset('assets/images/${cart.item.icon}', fit: BoxFit.cover),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Text(cart.item.name,
                  style: TextStyle(color: ColorHelper.white, fontSize: 16)),
              Row(
                children: [
                  Container(
                    padding: const EdgeInsets.all(3),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle, color: ColorHelper.tertiary),
                    child: GestureDetector(
                      onTap: () {
                        context.read<CartBloc>().add(CartItemDecreased(item: cart.item));
                      },
                      child: Icon(Icons.remove, color: ColorHelper.quaternary),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                        left: widthSize * 0.035, right: widthSize * 0.035),
                    child: Text(cart.quantity.toString(),
                        style: TextStyle(
                            fontWeight: FontWeight.w900,
                            fontSize: 20,
                            color: ColorHelper.white)),
                  ),
                  Container(
                    padding: const EdgeInsets.all(3),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle, color: ColorHelper.quaternary),
                    child: GestureDetector(
                      onTap: () {
                        context.read<CartBloc>().add(CartItemIncreased(item: cart.item));
                      },
                      child: Icon(Icons.add, color: ColorHelper.yellow),
                    ),
                  ),
                ],
              ),
            ],
          ),
          Text('\$ ${cart.item.price}',
              style: TextStyle(
                  fontSize: 22,
                  fontWeight: FontWeight.w600,
                  color: ColorHelper.yellow))
        ],
      ),
    );
  }
}
