import 'package:flutter/cupertino.dart';
import 'package:food_ordering_course/helper/color_helper.dart';
import 'package:food_ordering_course/routes/AppRoutes.dart';
import 'package:go_router/go_router.dart';

class EmptyCartWidget extends StatelessWidget {
  const EmptyCartWidget({super.key});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    return SizedBox(
      width: widthSize,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            margin: EdgeInsets.only(
                bottom: heightSize * 0.02, top: heightSize * 0.08),
            child: Image.asset('assets/images/shopping-bag.png',
                width: widthSize * 0.5),
          ),
          Text('Your cart is empty!',
              style: TextStyle(
                  color: ColorHelper.white,
                  fontWeight: FontWeight.w600,
                  fontSize: 22)),
          GestureDetector(
            onTap: () {
              context.go(AppRoutes.HOME);
            },
            child: Container(
              margin: EdgeInsets.only(top: heightSize * 0.02),
              padding: EdgeInsets.all(widthSize * 0.03),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: ColorHelper.yellow,
              ),
              child: Text('Order Something',
                  style: TextStyle(
                      color: ColorHelper.white, fontWeight: FontWeight.w600)),
            ),
          )
        ],
      ),
    );
  }
}
