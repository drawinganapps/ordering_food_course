import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:food_ordering_course/bloc/cart/cart_bloc.dart';
import 'package:food_ordering_course/bloc/item/item_bloc.dart';
import 'package:food_ordering_course/helper/color_helper.dart';
import 'package:food_ordering_course/models/cart_model.dart';
import 'package:food_ordering_course/models/item_model.dart';
import 'package:food_ordering_course/routes/AppRoutes.dart';
import 'package:go_router/go_router.dart';

class ItemCardWidget extends StatelessWidget {
  final ItemModel item;

  const ItemCardWidget({super.key, required this.item});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    return Container(
      padding: EdgeInsets.only(
          left: widthSize * 0.02,
          right: widthSize * 0.02,
          top: heightSize * 0.01,
          bottom: heightSize * 0.01),
      decoration: BoxDecoration(
          color: ColorHelper.secondary,
          borderRadius: BorderRadius.circular(20)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          /*BlocBuilder(builder: (context, state) {
            return
          }),*/
          GestureDetector(
            child: Image.asset(
              'assets/images/${item.icon}',
              width: widthSize * 0.35,
              fit: BoxFit.cover,
            ),
            onTap: () {
              context.read<ItemBloc>().add(ItemSelected(item: item));
              context.go(AppRoutes.DETAILS, extra: {'id': item.id});
            },
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.only(bottom: heightSize * 0.005),
                child: Text(item.name,
                    style: TextStyle(
                        color: ColorHelper.white,
                        fontWeight: FontWeight.w500,
                        fontSize: 18)),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('\$ ${item.price}',
                      style: TextStyle(
                          color: ColorHelper.white,
                          fontSize: 22,
                          fontWeight: FontWeight.w600)),
                  Container(
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: ColorHelper.yellow.withOpacity(0.3)),
                    padding: const EdgeInsets.all(3),
                    child: GestureDetector(
                      onTap: () {
                        context.read<CartBloc>().add(CartItemAdded(cart: CartModel(item: item, quantity: 1)));
                        final snackBar = SnackBar(
                          content: Text('${item.name} has been added to the cart!'),
                          backgroundColor: ColorHelper.yellow,
                          action: SnackBarAction(
                            label: 'Close',
                            textColor: ColorHelper.white,
                            onPressed: () {
                            },
                          ),
                        );
                        ScaffoldMessenger.of(context).showSnackBar(snackBar);
                      },
                      child: Icon(Icons.add_rounded, color: ColorHelper.yellow),
                    ),
                  )
                ],
              )
            ],
          )
        ],
      ),
    );
  }
}
