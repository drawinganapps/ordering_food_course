import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:food_ordering_course/bloc/cart/cart_bloc.dart';
import 'package:food_ordering_course/cubit/quantity_cubit.dart';
import 'package:food_ordering_course/helper/color_helper.dart';
import 'package:food_ordering_course/models/cart_model.dart';
import 'package:food_ordering_course/models/item_model.dart';
import 'package:food_ordering_course/routes/AppRoutes.dart';
import 'package:go_router/go_router.dart';

class ItemDetailActionWidget extends StatelessWidget {
  final ItemModel itemModel;

  const ItemDetailActionWidget({super.key, required this.itemModel});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    return Container(
      padding: EdgeInsets.only(
          left: widthSize * 0.05,
          right: widthSize * 0.05,
          bottom: heightSize * 0.03,
          top: heightSize * 0.01),
      height: heightSize * 0.12,
      child: BlocProvider(
          create: (_) => QuantityCubit(),
          child: BlocBuilder<QuantityCubit, int>(builder: (context, state) {
            return Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Container(
                        padding: const EdgeInsets.all(3),
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: ColorHelper.tertiary),
                        child: GestureDetector(
                          onTap: () {
                            context.read<QuantityCubit>().decrement();
                          },
                          child:
                              Icon(Icons.remove, color: ColorHelper.quaternary),
                        )),
                    Container(
                      margin: EdgeInsets.only(
                          left: widthSize * 0.035, right: widthSize * 0.035),
                      child: Text(state.toString(),
                          style: TextStyle(
                              fontWeight: FontWeight.w900,
                              fontSize: 25,
                              color: ColorHelper.white)),
                    ),
                    Container(
                      padding: const EdgeInsets.all(3),
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: ColorHelper.quaternary),
                      child: GestureDetector(
                        onTap: () {
                          context.read<QuantityCubit>().increment();
                        },
                        child: Icon(Icons.add, color: ColorHelper.yellow),
                      ),
                    ),
                  ],
                ),
                GestureDetector(
                  onTap: () {
                    context.read<CartBloc>().add(CartItemAdded(
                        cart: CartModel(item: itemModel, quantity: state)));
                    var snackBar = SnackBar(
                        content: Text(
                            '${itemModel.name} has been added to the cart!'),
                        backgroundColor: ColorHelper.yellow,
                        action: SnackBarAction(
                          label: 'View',
                          onPressed: () {
                            context.go(AppRoutes.ORDERS);
                          },
                          textColor: ColorHelper.white,
                        ));
                    ScaffoldMessenger.of(context).showSnackBar(snackBar);
                  },
                  child: Container(
                    width: widthSize * 0.5,
                    padding: EdgeInsets.only(
                        top: heightSize * 0.025,
                        bottom: heightSize * 0.025,
                        left: widthSize * 0.08,
                        right: widthSize * 0.08),
                    decoration: BoxDecoration(
                        color: ColorHelper.yellow,
                        borderRadius: BorderRadius.circular(10)),
                    child: Center(
                      child: Text('Add to Cart',
                          style: TextStyle(
                              color: ColorHelper.white,
                              fontSize: 18,
                              fontWeight: FontWeight.w600)),
                    ),
                  ),
                ),
              ],
            );
          })),
    );
  }
}
