import 'package:food_ordering_course/routes/AppRoutes.dart';
import 'package:food_ordering_course/screens/home_screen.dart';
import 'package:food_ordering_course/screens/item_detail_screen.dart';
import 'package:food_ordering_course/screens/order_completed_screen.dart';
import 'package:food_ordering_course/screens/order_screen.dart';
import 'package:go_router/go_router.dart';

class AppPage {
// GoRouter configuration
  static var routes = GoRouter(
    initialLocation: AppRoutes.HOME,
    routes: [
      GoRoute(
        path: AppRoutes.HOME,
        builder: (context, state) => const HomeScreen(),
      ),
      GoRoute(
        path: AppRoutes.DETAILS,
        builder: (context, state) => const ItemDetailScreen(),
      ),
      GoRoute(
        path: AppRoutes.ORDERS,
        builder: (context, state) => const OrderScreen(),
      ),
      GoRoute(
        path: AppRoutes.ORDER_COMPLETE,
        builder: (context, state) => const OrderCompletedScreen(),
      ),
    ],
  );
}
