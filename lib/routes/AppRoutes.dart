class AppRoutes {
  static const String HOME = '/home';
  static const String DETAILS = '/details';
  static const String ORDERS = '/orders';
  static const String ORDER_COMPLETE = '/complete';
}
